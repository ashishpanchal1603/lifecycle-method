import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Developer extends Component {
  constructor (props) {
    super(props)
    this.state = {
      name: 'ashish'
    }
  }

  componentDidUpdate (prevProp, prevState) {
    if (prevProp.data !== this.props.data) {
      console.log('prevProp.data', prevProp.data)
    }
  }

  componentWillUnmount () {
    console.log('componentWillUnmount')
  }

  render () {
    return (
      <div>{this.props.data}</div>
    )
  }
}

Developer.propTypes = {
  data: PropTypes.number
}
