import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Developer from './Developer'
export default class LifeCycle extends Component {
  constructor (props) {
    super(props)
    this.state = {
      count: 0,
      position: 'Jr.Developer',
      show: true
    }
    console.log('construction calling')
  }

  static getDerivedStateFromProps (props, state) {
    console.log('props', props)
    console.log('state :>> ', state)

    // this is change but no change in componentDidMount
    // return {
    //   count :100;
    // }
    return null
  }

  componentDidMount (props, state) {
    setTimeout(() => {
      this.setState({ position: 'Sr.Developer' })
    }, 2000)
  }

  componentDidUpdate (prevProps, prevState) {
    console.log('prevState :>> ', prevState)
    if (prevState.count !== this.state.count) {
      console.log('componentDidUpdate')
    }
  }

  render () {
    return (
      <>
      <h1>{this.props.name}</h1>
      <h1>{this.state.count}</h1>
      <h1>{this.state.position}</h1>
      {this.state.show && <Developer data={this.state.count}/>}
      <button type='button' onClick={() => { this.setState({ count: this.state.count + 1 }) }}>Button</button>
      <button type='button' onClick={() => { this.setState({ show: !this.state.show }) }}>Show-Hide</button>
      </>
    )
  }
}

LifeCycle.propTypes = {
  name: PropTypes.string.isRequired,
  data: PropTypes.number
}
